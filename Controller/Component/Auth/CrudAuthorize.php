<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('BaseAuthorize', 'Controller/Component/Auth');
App::uses('Router', 'Routing');

/**
 * An authorization adapter for AuthComponent. Provides the ability to authorize using CRUD mappings.
 * CRUD mappings allow you to translate controller actions into *C*reate *R*ead *U*pdate *D*elete actions.
 * This is then checked in the AclComponent as specific permissions.
 *
 * For example, taking `/posts/index` as the current request. The default mapping for `index`, is a `read` permission
 * check. The Acl check would then be for the `posts` controller with the `read` permission. This allows you
 * to create permission systems that focus more on what is being done to resources, rather than the specific actions
 * being visited.
 *
 * @package       AuthManager.Controller.Component.Auth
 * @since 0.9.0
 * @see AuthComponent::$authenticate
 * @see AclComponent::check()
 */
class CrudAuthorize extends BaseAuthorize {

/**
 * Sets up additional actionMap values that match the configured `Routing.prefixes`.
 *
 * @param ComponentCollection $collection The component collection from the controller.
 * @param string $settings An array of settings. This class does not use any settings.
 */
	public function __construct(ComponentCollection $collection, $settings = array()) {
		parent::__construct($collection, $settings);
		$this->_setPrefixMappings();
	}

/**
 * sets the crud mappings for prefix routes.
 *
 * @return void
 */
	protected function _setPrefixMappings() {
		$crud = array('create', 'read', 'update', 'delete');
		$map = array_combine($crud, $crud);

		$prefixes = Router::prefixes();
		if (!empty($prefixes)) {
			foreach ($prefixes as $prefix) {
				$map = array_merge($map, array(
					$prefix . '_index' => 'read',
					$prefix . '_add' => 'create',
					$prefix . '_edit' => 'update',
					$prefix . '_view' => 'read',
					$prefix . '_remove' => 'delete',
					$prefix . '_create' => 'create',
					$prefix . '_read' => 'read',
					$prefix . '_update' => 'update',
					$prefix . '_delete' => 'delete'
				));
			}
		}
		$this->mapActions($map);
	}

/**
 * Authorize a user using the mapped actions and the AclComponent.
 *
 * @param array $user The user to authorize
 * @param CakeRequest $request The request needing authorization.
 * @return bool
 */
	public function authorize($user, CakeRequest $request) {
            $user = array($this->settings['userModel'] => $user);
            $Acl = $this->_Collection->load('Acl');
            
            //FIX
            if (in_array(Inflector::pluralize(Inflector::classify($request->params['controller'])), Configure::read('AuthManager.ignoreControllers'))) {
                return true;
            }

        //debug($this->settings['actionMap'][$request->params['action']]);
            
            //CakeLog::debug(var_export( $this->action($request, ':controller'), true));
           // CakeLog::debug(var_export( $this->settings['actionMap'][$request->params['action']], true));
                        
            //Vérification pour les droits hors CRUD
            //CakeLog::debug(var_export( $request->params, true));
            if (!isset($this->settings['actionMap'][$request->params['action']])) {
                    
                    /*CakeLog::debug(var_export( $Acl->check(
			$user,
			 $request->params['action']
                    , 'read'), true));*/
                    return $Acl->check(
			$user,
			$request->params['action'], 'read'
                    );
                    
			/*trigger_error(__d('cake_dev',
				'CrudAuthorize::authorize() - Attempted access of un-mapped action "%1$s" in controller "%2$s"',
				$request->action,
				$request->controller
				),
				E_USER_WARNING
			);
                    return false;*/
		}
                
                //Vérification pour les controlleurs simples avec actions supplémentaires
                if(in_array($this->settings['actionMap'][$request->params['action']], $this->settings['actionMap'])
                        && !in_array($this->settings['actionMap'][$request->params['action']], array('read','create','update','delete'))
                        )
                {
                    return $Acl->check(
                          $user,
                          $this->action($request, ':controller') . '/' . $this->settings['actionMap'][$request->params['action']],
                          'read'
                    );
                    /*return $Acl->check(
                          $user,
                          $this->action($request, ':controller').'/'.array_search($request->params['action'], $this->settings['actionMap']),
                          '*'
                    );  */
                }    
                
                //Vérification avec les droits simples de CrudAuthorize
		return $Acl->check(
			$user,
			$this->action($request, ':controller'),
			$this->settings['actionMap'][$request->params['action']]
		);
	}
}
