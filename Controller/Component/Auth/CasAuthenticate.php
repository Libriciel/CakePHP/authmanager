<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::import('Vendor', 'AuthManager.jasig/phpcas/CAS');
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

/**
 * Authentification via LDAP.
 *
 * @package AuthManager.Controller.Component.Auth
 */

class CasAuthenticate extends BaseAuthenticate {

    function __construct(ComponentCollection $collection, $settings = array()) {
        
        if (Configure::read('debug') > 0) {
            phpCAS::setLogger(LOGS . 'Cas.log');
        }
        
        if(CasAuthenticate::used())
        { 
            phpCAS::client(CAS_VERSION_2_0, Configure::read('AuthManager.Cas.host'), intval(Configure::read('AuthManager.Cas.port')), Configure::read('AuthManager.Cas.uri'));
        
            $certServer = Configure::read('AuthManager.Cas.cert_path');
            if (file_exists($certServer)) {
                phpCAS::setCasServerCACert($certServer);
            } else {
                phpCAS::setNoCasServerValidation();
            }
            
            //phpCAS::allowProxyChain(new CAS_ProxyChain(array('https://cas-ecoll.test.adullact.org/cas/')));
        }
        
        parent::__construct($collection, $settings);
    }

    public function authenticate(CakeRequest $request, CakeResponse $response) {
        if(!CasAuthenticate::used())
            return false;
        
        phpCAS::handleLogoutRequests(false);
        phpCAS::forceAuthentication();
        
        $user=$this->_findUser(phpCAS::getUser());
        $user['AuthManager.Authentification.type'] = 'CAS';
        
        return $user;
    }

    public function unauthenticated(CakeRequest $request, CakeResponse $response) {
//Call Auth->login() to set default auth session variables
        if (!empty($this->_Collection)) {
            $controller = $this->_Collection->getController();
            if (!empty($controller->Auth)) {
                $login = $controller->Auth->login(); //This will eventually call back in to $this->authenticate above, thus triggering CAS as needed
                if (!empty($login)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function logout($user) {
        if(!CasAuthenticate::used() || !isset($user['AuthManager.Authentification.type']))
            return false;
        
        if ($user['AuthManager.Authentification.type'] == 'CAS' && phpCAS::isAuthenticated()) {
            $controller = $this->_Collection->getController();
            $controller->Auth->logoutRedirect = $this->settings['logoutRedirect'];
            phpCAS::logout(array('service' => Router::url($this->settings['loginAction'], true)));
        } else {
        }
    }
    
    public static function used()
    {
        if (!Configure::read('AuthManager.Authentification.use') || Configure::read('AuthManager.Authentification.type') !== 'CAS') {
            return false;
        } else {
            return true;
        }
    }
}
