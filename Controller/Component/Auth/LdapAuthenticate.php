<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 * 
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('BaseAuthenticate', 'Controller/Component/Auth');

/**
 * Authentification via LDAP.
 *
 * @package AuthManager.Controller.Component.Auth
 */

class LdapAuthenticate extends BaseAuthenticate {
    
    protected $_use_log = false;
    
/**
 * Constructor
 *
 * @param ComponentCollection $collection The Component collection used on this request.
 * @param array $settings Array of settings to use.
 */
	
	function __construct(ComponentCollection $collection, $settings = array()) {
            if (Configure::read('debug') > 0) {
                $this->_use_log=true;
            }
            
            parent::__construct($collection, $settings);
        }
        
/**
 * Checks the fields to ensure they are supplied.
 *
 * @param CakeRequest $request The request that contains login information.
 * @param string $model The model used for login verification.
 * @param array $fields The fields to be checked.
 * @return bool False if the fields have not been supplied. True if they exist.
 */
	protected function _checkFields(CakeRequest $request, $model, $fields) {
		if (empty($request->data[$model])) {
			return false;
		}
		foreach (array($fields['username']) as $field) {
			$value = $request->data($model . '.' . $field);
			if (empty($value) && $value !== '0' || !is_string($value)) {
				return false;
			}
		}
		return true;
	}
 /**
 * Authentication hook to authenticate a user against an LDAP server.
 *
 * @param CakeRequest $request The request that contains login information.
 * @param CakeResponse $response Unused response object.
 * @return mixed. False on login failure. An array of User data on success.
 */   
    public function authenticate(CakeRequest $request, CakeResponse $response) {
        if(LdapAuthenticate::used())
        { 
            $userModel = $this->settings['userModel'];
            list(, $model) = pluginSplit($userModel);

            $fields = $this->settings['fields'];

            if (!$this->_checkFields($request, $model, $fields)) {
                    return false;
            }
            if(!empty($this->settings['except']) && in_array($request->data[$model][$fields['username']], $this->settings['except'])){
                return false;
            }

            try{
                $oLdap = ClassRegistry::init('LdapManager.LdapUser');
                if(!empty($oLdap) && !empty($request->data[$model][$fields['password']]))
                {
                    if($oLdap->isAuthenticate($request->data[$model][$fields['username']], $request->data[$model][$fields['password']])===true){
                        return $this->_findUser($request->data[$model][$fields['username']]);
                    }
                }
            }
            catch (Exception $e)
            {
                CakeLog::error($e->getLine() . $e->getMessage());
            }
        }
        
        return false;
    }
    
    public static function used()
    {
        if (!Configure::read('AuthManager.Authentification.use') || Configure::read('AuthManager.Authentification.type') !== 'LDAP') {
            return false;
        } else {
            return true;
        }
    }
}