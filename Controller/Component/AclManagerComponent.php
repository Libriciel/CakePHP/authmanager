<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('Component', 'Controller');

/**
 * Authentification via LDAP.
 *
 * @package AuthManager.Controller.Component
 */

class AclManagerComponent extends Component {

    public $components = array('Acl');

    protected $acos = array();

    /**
    * Request object
    *
    * @var CakeRequest
    */
    public $_Instance;

    /**
     * Initializes AuthComponent for use in the controller.
     *
     * @param Controller $controller A reference to the instantiating controller object
     * @return void
     */

    function initialize(Controller $controller) {

        $this->_Instance = $controller;
    }

    /**
     * Called automatically after controller beforeFilter
     * @param Controller $controller A reference to the controller object
     */
    function startup(Controller $controller) {

           /**
            * Loading required Model
            */
           $aros = Configure::read('AuthManager.models');

           foreach ($aros as $aro) {
                list($plugin, $class) = pluginSplit($aro);
                if ($plugin) {
                    $this->{$plugin} = ClassRegistry::init($plugin);
                    $this->{$plugin}->{$class} = ClassRegistry::init($aro);
                }
                else
                {
                    $this->{$aro} = ClassRegistry::init($aro);
                }
           }
    }

    public function __call($name, $arguments) {
        //FIX A CHANGER
        $position=strripos(Inflector::underscore($name),'_');
        $position=($position+1)-substr_count(Inflector::underscore($name), '_');
        $function=substr($name, 0, $position);
        array_unshift($arguments, substr($name, $position));
        if (method_exists($this, $function))
            return call_user_func_array(array($this, $function), $arguments);
        else {
            throw new Exception(sprintf('The required method "%s" does not exist for %s', $name, get_class()));
        }
    }

     /**
     * Returns all the ACOs including their path
     */
    protected function _getAcos() {
            $acos = $this->Acl->Aco->find('all', array('order' => 'Aco.lft ASC', 'recursive' => -1));
            $parents = array();
            foreach ($acos as $key => $data) {

                    $aco =& $acos[$key];
                    $id = $aco['Aco']['id'];

                    // Generate path
                    if ($aco['Aco']['parent_id'] && isset($parents[$aco['Aco']['parent_id']])) {
                            $parents[$id] = $parents[$aco['Aco']['parent_id']] . '/' . $aco['Aco']['alias'];
                    } else {
                            $parents[$id] = $aco['Aco']['alias'];
                    }
                    $aco['Aco']['action'] = $parents[$id];
            }
            return $acos;
    }

    /**
    * Returns permissions keys in Permission schema
    * @see DbAcl::_getKeys()
    */
   protected function _getKeys() {
        $keys = $this->Acl->Aro->Permission->schema();
        $newKeys = array();
        $keys = array_keys($keys);
        foreach ($keys as $key) {
                if (!in_array($key, array('id', 'aro_id', 'aco_id'))) {
                        $newKeys[] = $key;
                }
        }
        return $newKeys;
   }

   /**
    * Returns permissions keys in Permission schema
    * @see DbAcl::_getKeys()
    */
   public function getKeys() {
        return $this->_getKeys();
   }

   /**
 * loading test controller
 *
 * @param array $settings
 * @return Controller
 */
    protected function _loadController($settings = array()) {
            $request = new CakeRequest(Router::url($settings), false);
            $request->addParams($settings);
            $oController = new Controller($request);
            //$oController->uses = null;
            $oController->uses = 'Acteur';
            $oController->components = array('Auth');
            $oController->Components->init($oController);

            //$oController->constructClasses(); //FIX
            //debug($oController->Components->getController());
            //exit;
            $oController->Components->trigger('initialize', array($oController));
            return $oController;
    }

   /**
    * Gets the action from Authorizer
    */
   protected function _action($controller, $path = '/:plugin/:controller/:action') {

        $params = array_merge(array('admin' => false, 'controller' => $controller, 'action' => null, 'plugin' => null));
        $oController=$this->_loadController($params);
        debug($oController);
        exit;
        $plugin = empty($request['plugin']) ? null : Inflector::camelize($request['plugin']) . '/';

        $request = new CakeRequest(null, false);
        $request->addParams($params);


        App::uses($controller, 'Controller');
        $oController = new $controllerName($request);
        //$oController->Components->init( $oController );
        //$oController->Auth->initialize( $oController );

        $collection = new ComponentCollection();
        $collection->init($oController);
        $this->Auth = new AuthComponent($collection);

        $oController->Components->init($oController);

        debug($oController->Auth);


        exit;
        //$oController->Components->load('Auth');
        //$oController->constructClasses()
        //$oController->Auth->initialize( $oController );
        //$authorizer = $this->_getAuthorizer($oController);
        //debug($authorizer);
        exit;
        return $authorizer->action($request, $path);
   }

   /**
    * Gets the Authorizer object from Auth
    */
   protected function _getAuthorizer($oController) {
           //debug($oController->Auth);
           $authorzeObjects = $oController->Auth->_authorizeObjects;
           debug($authorzeObjects);
           foreach ($authorzeObjects as $object) {
                   if (!$object instanceOf ActionsAuthorize) {
                           continue;
                   }
                   $oController->_authorizer = $object;
                   break;
           }

           return $oController->_authorizer;
   }

	/**
	 * Returns all the controllers from Cake and Plugins
	 * Will only browse loaded plugins
	 *
	 * @return array('Controller1', 'Plugin.Controller2')
	 */
	protected function _getControllers() {

		// Getting Cake controllers
		$objects = array('Cake' => array());
		$objects['Cake'] = App::objects('Controller');
		$unsetIndex = array_search("AppController", $objects['Cake']);
		if ($unsetIndex !== false) {
			unset($objects['Cake'][$unsetIndex]);
		}

		// App::objects does not return PagesController
		if (!in_array('PagesController', $objects['Cake'])) {
		    array_unshift($objects['Cake'], 'PagesController');
		}

		// Getting Plugins controllers
		$plugins = CakePlugin::loaded();
		foreach ($plugins as $plugin) {
			$objects[$plugin] = App::objects($plugin . '.Controller');
			$unsetIndex = array_search($plugin . "AppController", $objects[$plugin]);
			if ($unsetIndex !== false) {
				unset($objects[$plugin][$unsetIndex]);
			}
		}

		// Around each controller
		$return = array();
		foreach ($objects as $plugin => $controllers) {
			$controllers = str_replace("Controller", "", $controllers);
			foreach ($controllers as $controller) {
				if ($plugin !== "Cake") {
					$controller = $plugin . "." . $controller;
				}
				if (App::import('Controller', $controller)) {
					$return[] = $controller;
				}
			}
		}

		return $return;
	}



   /**
    * Recursive function to find permissions avoiding slow $this->Acl->check().
    */
    private function _evaluate_permissions($permKeys, $aro, $aco, $aco_index) {
        //$permissions = Set::extract("/Aro[model={$aro['alias']}][foreign_key={$aro['id']}]/Permission/.", $aco);
        $permissions = Set::extract("/Aro[model={$aro['alias']}][foreign_key={$aro['id']}]/Permission/.", $aco);
        if (empty($permissions))
        {
            return array(
                'allowed' => false,
                'inherited' => false,
            );
        }

        //debug('{n}.Aro[' . $aro['alias'] . '].id['.$aro['id'].']');
        //$permissions = Hash::extract($aco, '{n}.Aro[' . $aro['alias'] . '].id['.$aro['id'].']');
        //debug($permissions);
        $allowed = false;
        $inherited = false;
        $inheritedPerms = array();
        $allowedPerms = array();

        /**
         * Manually checking permission
         * Part of this logic comes from DbAcl::check()
         */

        foreach ($permKeys as $key) {
                if (!empty($permissions[0])) {
                        if ($permissions[0][$key] == -1) {
                                $allowed = false;
                                break;
                        } elseif ($permissions[0][$key] == 1) {
                                $allowedPerms[$key] = 1;
                        } elseif ($permissions[0][$key] == 0) {
                                $inheritedPerms[$key] = 0;
                        }
                } else {
                        $inheritedPerms[$key] = 0;
                }
        }

        if (count($allowedPerms) === count($permKeys)) {
                $allowed = true;
        } elseif (count($inheritedPerms) === count($permKeys)) {
                if ($aco['Aco']['parent_id'] == null) {
                        $this->lookup +=1;
                        $acoNode = (isset($aco['Action'])) ? $aco['Action'] : null;
                        $aroNode = array('model' => $aro['alias'], 'foreign_key' => $aro['id']);
                        try{
                            $allowed = $this->Acl->check($aroNode, $acoNode);
                        }
                        catch (Exception $e)
                        {
                            $allowed=false;
                        }
                        $this->acos[$aco_index]['evaluated'][$aro['id']] = array(
                                'allowed' => $allowed,
                                'inherited' => true
                        );
                }
                else {
                        /**
                         * Do not use Set::extract here. First of all it is terribly slow,
                         * besides this we need the aco array index ($key) to cache are result.
                         */
                        foreach ($this->acos as $key => $a) {
                                if ($a['Aco']['id'] == $aco['Aco']['parent_id']) {
                                        $parent_aco = $a;
                                        break;
                                }
                        }
                        // Return cached result if present
                        if (isset($parent_aco['evaluated'][$aro['id']])) {
                                return $parent_aco['evaluated'][$aro['id']];
                        }
                        // Perform lookup of parent aco
                        $evaluate = $this->_evaluate_permissions($permKeys, $aro, $parent_aco, $key);

                        // Store result in acos array so we need less recursion for the next lookup
                        $this->acos[$key]['evaluated'][$aro['id']] = $evaluate;
                        $this->acos[$key]['evaluated'][$aro['id']]['inherited'] = true;

                        $allowed = $evaluate['allowed'];
                }
                $inherited = true;
        }

        return array(
                'allowed' => $allowed,
                'inherited' => $inherited,
        );
    }

    /**
    * Manage Permissions
    */
   public function permissions($model, $id, $plugin=null, $permissions=null, $prefix=null, $filter=array()) {

        if (empty($plugin)) {
            $Aro = $this->{$model};
        } else {
            $Aro = $this->{$plugin}->{$model};
        }

        $Aro->recursive=-1;
        $Aro->id=$id;
        $Aro->read();
        $permKeys = $this->_getKeys();
        if (!empty($permissions)) {
            $Keys=array();
            foreach($permissions as $permission)
            {
                $Keys[]='_'.$permission;
            }
            $permKeys = array_intersect($permKeys, $Keys);
        }
        /**
         *
         * Build permissions info
         */
        $configCRUD=Configure::read('AuthManager.configCRUD');
        $this->Acl->Aco->Behaviors->load('Containable');
        $conditions = array();
        $joins = array();
        $conditionModel=array();

        if(in_array($model, Configure::read('AuthManager.aros'))){
            $conditionModel['Aro.model']=$model;
        }
        else{
            $this->_active($Aro, array('active','actif'), $joins, $conditions);
        }


        if(empty($conditions['Aco.model'])) {
            $conditions[] = array(
                'Aco.model' => !in_array($model, Configure::read('AuthManager.aros')) ? $model : null,
            );
        }

        ${'acos_'.$model} = $this->Acl->Aco->find('all', array(
            'contain' => array(
                'Aro' => array(
                    'conditions' => $conditionModel+=array(
                        'Aro.foreign_key =' => $id))),
            'joins' => $joins,
            'conditions' => $conditions,
            'order' => 'Aco.lft ASC',
            'recursive' => -1));
        $perms = array();
        $parents = array();

        foreach (${'acos_'.$model}  as $key => $data) {
                $aco =& ${'acos_'.$model}[$key];
                $aco = array('Aco' => $data['Aco'], 'Aro' => $data['Aro'], 'Action' => array());
                $aco_id = $aco['Aco']['id'];

                // Generate path
                if ($aco['Aco']['parent_id'] && isset($parents[$aco['Aco']['parent_id']])) {
                        $parents[$aco_id] = $parents[$aco['Aco']['parent_id']] . '/' . $aco['Aco']['alias'];
                } else {
                        $parents[$aco_id] = $aco['Aco']['alias'];
                }
                $aco['Action'] = $parents[$aco_id];

                //Echap droits non accessible
                if(!empty($filter['role']) && !empty($configCRUD['CRUD'][$aco['Aco']['alias']]['role']) && !in_array($filter['role'], $configCRUD['CRUD'][$aco['Aco']['alias']]['role']))
                {
                    unset(${'acos_'.$model}[$key]);
                    continue;
                }

                //Affiche les droits que le manager peut modifier sur un utilisateur
                if($prefix=='manager'){
                    //$node=array("$model" => array('id' => $data['Aco']['id']));
                    if(!$this->Acl->check(array('User' => array('id' => AuthComponent::user('id'))), $aco['Action'], 'manager'))
                    {
                        unset(${'acos_'.$model}[$key]);
                        continue;
                    }
                }

                // Fetching permissions per ARO
                $acoNode = $aco['Action'];
                //Gestion des erreurs dans le formulaire principal
                if(!empty($this->_Instance->request->data['Aco'][$model])){
                    foreach ($this->_Instance->request->data['Aco'][$model][$aco_id]['permKeys'] as $key_aro =>$aro) {
                        ${'acos_'.$model}[$key]['Aro'][0]['Permission'][$key_aro]=$aro;
                    }
                }
                foreach($aco['Aro'] as $aro) {
                    $aroId = $Aro->{$Aro->primaryKey};//$aro[$Aro->primaryKey];

                    $evaluate = $this->_evaluate_permissions($permKeys, array('id' => $aroId, 'alias' => $Aro->alias), $aco, $key);
                    //$perms[$acoNode][$Aro->alias . ":" . $Aro->primaryKey . '-inherit'] = $evaluate['inherited'];
                    $perms[$acoNode][$Aro->alias . ":" . $Aro->primaryKey] = $evaluate['allowed'];
                    $perms[$acoNode]['foreign_key'] = $aco['Aco']['foreign_key'];
                    $perms[$acoNode]['parent_id'] = $aco['Aco']['parent_id'];
                }

        }

 //$this->_Instance->presetVars[$key]
        $this->_Instance->request->data['Perms'][$Aro->alias] = $perms;
        $this->_Instance->set('permKeys_'.$Aro->alias, $permKeys);
        $this->_Instance->set('acoAlias_'.$Aro->alias, $Aro->alias);
        //$this->_Instance->set('acoAlias.'.$Aro->alias, $Aro->alias);
        $this->_Instance->set('aroDisplayField_'.$Aro->alias, $Aro->displayField);
        $this->_Instance->set(compact('acos_'.$model/*, 'aros'*/));

        $this->_Instance->set('controllers_list', $this->_getControllers());
        /*

        $this->request->data = array('Perms' => $perms);
        $this->set('aroAlias', $Aro->alias);
        $this->set('aroDisplayField', $Aro->displayField);
        $this->set(compact('acos', 'aros'));*/
   }

   /**
    * Manage Permissions
    */
   public function setPermissions($model, $modelCible, $id, $acos) {
        // Saving permissions
        $nodecible = array("$modelCible" => array('id' => $id));

        foreach ($acos as $aco_id => $aco) {
            if(!empty($aco['foreign_key']))
            $node=array("$model" => array('id' => $aco['foreign_key']));
            else
               $node=$aco['alias'];

            foreach($aco['permKeys'] as $key=>$perm)
            {
                if ($key{0} == '_') {
                        $key = substr($key, 1);
                }
                if ($perm == 1) {
                    $this->Acl->allow($nodecible, $node, $key);
                }
//                elseif ($perm == 'inherit') {
//                        $this->Acl->inherit($node, $aco['alias']);
//                }
//                elseif ($perm == -1) {
//                    $this->Acl->deny($nodecible, $node, $key);
//                }
                elseif ($perm == 0) {
                    $this->Acl->deny($nodecible, $node, $key);
                }
            }
        }
   }

   function setAro($model, $name, $parent_id)
   {
        $Aro = $this->{$model};
        $Model->create();
        $Model->id = $item['id'];
        exit;
        try {
                $node = $Model->node();
        } catch (Exception $e) {
                $node = false;
        }

            // Node exists
            if ($node) {
                    $parent = $Model->parentNode();
                    if (!empty($parent)) {
                            $parent = $Model->node($parent, $type);
                    }
                    $parent = isset($parent[0][$type]['id']) ? $parent[0][$type]['id'] : null;

                    // Parent is incorrect
                    if ($parent != $node[0][$type]['parent_id']) {
                            // Remove Aro here, otherwise we've got duplicate Aros
                            // TODO: perhaps it would be nice to update the Aro with the correct parent
                            $this->Acl->Aro->delete($node[0][$type]['id']);
                            $node = null;
                    }
            }

            // Missing Node or incorrect
            if (empty($node)) {

                    // Extracted from AclBehavior::afterSave (and adapted)
                    $parent = $Model->parentNode();
                    if (!empty($parent)) {
                            $parent = $Model->node($parent, $type);
                    }
                    $data = array(
                            'parent_id' => isset($parent[0][$type]['id']) ? $parent[0][$type]['id'] : null,
                            'model' => $Model->name,
                            'foreign_key' => $Model->id
                    );

                    // Creating ARO
                    $this->Acl->{$type}->create($data);
                    $this->Acl->{$type}->save();
                    $count++;
            }
    }

    private function _active($Aro, $aKey, &$joins, &$conditions){
        foreach ($aKey as $key => $value) {
            if (array_key_exists($value, $Aro->schema())) {
                $joins[] =
                    array(
                        'table' => $Aro->tablePrefix.$Aro->useTable,
                        'alias' => $Aro->tablePrefix.$Aro->alias,
                        'type' => 'LEFT',
                        'conditions' => array(
                            "$Aro->tablePrefix$Aro->alias.id = Aco.foreign_key",
                        )
                );
                $conditions[] = array($Aro->tablePrefix.$Aro->alias.".$value" => true);

            }
        }
    }
}
