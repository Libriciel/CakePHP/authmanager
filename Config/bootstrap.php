<?php
/**
 * Acl Manager
 *
 * A CakePHP Plugin to manage Acl
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Frédéric Massart - FMCorz.net
 * @copyright     Copyright 2011, Frédéric Massart
 * @link          http://github.com/FMCorz/AuthManager
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/**
 * List of AROs (Class aliases)
 * Order is important! Parent to Children
 */
//Configure::write('AuthManager.aros', array('Role', 'User'));

/**
 * Limit used to paginate AROs
 * Replace {alias} with ARO alias
 * Configure::write('AuthManager.{alias}.limit', 3)
 */
// Configure::write('AuthManager.Role.limit', 3);

/**
 * Routing Prefix
 * Set the prefix you would like to restrict the plugin to
 * @see Configure::read('Routing.prefixes')
 */
// Configure::write('AuthManager.prefix', 'admin');

/**
 * Ugly identation?
 * Turn off when using CSS
 */
Configure::write('AuthManager.uglyIdent', true);
				
/**
 * Actions to ignore when looking for new ACOs
 * Format: 'action', 'Controller/action' or 'Plugin.Controller/action'
 */
Configure::write('AuthManager.ignoreActions', array('isAuthorized'));

/**
 * List of ARO models to load
 * Use only if AuthManager.aros aliases are different than model name
 */
// Configure::write('AuthManager.models', array('Group', 'Customer'));

/**
 * END OF USER SETTINGS
 */

Configure::write("AuthManager.version", "1.2.5");
if (!is_array(Configure::read('AuthManager.aros'))) {
	Configure::write('AuthManager.aros', array(Configure::read('AuthManager.aros')));
}
if (!is_array(Configure::read('AuthManager.ignoreActions'))) {
	Configure::write('AuthManager.ignoreActions', array(Configure::read('AuthManager.ignoreActions')));
}
if (!Configure::read('AuthManager.models')) {
	Configure::write('AuthManager.models', Configure::read('AuthManager.aros'));
}
