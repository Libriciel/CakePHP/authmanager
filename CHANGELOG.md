# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/) et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [4.0.0] - 2024-01-04

### Evolutions
- Changement de la license open source vers MIT
- Prise en charge de la diffusion avec packagist

## [3.0.2]

### Corrections
- Fix ajout du parametre de certificat CAS si un fichier de certificat existe

## [3.0.1]

### Corrections
- Fix phpCAS::setDebug() is deprecated in favor of phpCAS::setLogger()

## [3.0.0]

### Ajouts
- Minimum require php7.2

### Corrections
- Fix fontawesome 5

## [2.1.2]

### Evolutions
- Changement de nom de la librairie CAS pour apereo/phpCAS

## [2.1.1]

### Corrections
- Fix debug

## [2.1.0]

### Ajouts
- Prise en charge de ReflectionClass->ReflectionMethod au lieu de strpos '_' pour l'identification des fonctions privée dans la récupération des ACL ~Core

### Corrections
- Authentification CAS inopérante #3 ~CAS

## [2.0.0]

### Ajouts
- Prise en charge de php en version 7 ~Evolution ~Core

## [1.0.1]

### Corrections
- Appel de fonction check dans PermissionHelper
- Mise en commentaire de Proxy cas

## [1.0.0]

###  Ajout
- Mise en production