/**
 * Comportement des checkboxes / master checkbox
 * Select All / Deselect All
 * Initial state
 * @version 1.1
 */

define(["jquery"], function ($) {
    'use strict';
    
    // Main function
    $.fn.appAuthManager = function (options) {
        // Ensure that only one scrollUp exists
        if (!$.data(document.body, 'appAuthManager')) {
            $.data(document.body, 'appAuthManager', true);
            $.fn.appAuthManager.init(options);
        }
    };

    // Init
    $.fn.appAuthManager.init = function (options) {
        
        // La master checkbox coche/décoche tout d'un coup
        $(".masterCheckboxAuthManager").change(function () {
            $(".tableAco"+$(this).attr("data-model")+" input[type=checkbox]").not(':disabled').prop('checked', $(this).prop('checked'));
        });
        //Checkbox -> masterCheckbox  data-aco
        $(".childCheckboxAuthManager").change(function () {
            $(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] input[type=checkbox]").not(':disabled').prop('checked', $(this).prop('checked'));
        });

        $(".AuthManage_groupAllCRUDCheckbox").change(function () {
            console.log(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] :input[type=checkbox][readonly='readonly']");
            $(".tableAco"+$(this).attr("data-model")+" tr[data-aco_id="+$(this).attr("data-aco_id")+"] :input[type=checkbox][readonly='readonly']").prop('checked', $(this).prop('checked'));
        });
    }   
    
    $.appAuthManager = $.fn.appAuthManager;
});