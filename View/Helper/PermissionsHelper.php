<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

App::uses('HtmlHelper', 'View/Helper');
App::uses('Inflector', 'Utility');
App::import('Component', 'Acl');

/**
 *
 * Helper CakePHP to create Twitter Bootstrap elements
 * @author AWL
 *
 */
class PermissionsHelper extends AppHelper
{
    public $helpers = ['Session'];

    private $Acl = null;

    public function __construct(View $view, $settings = [])
    {
        $collection = new ComponentCollection();
        $this->Acl = new AclComponent($collection);
        parent::__construct($view, $settings);
    }

    public function check($path, $permission='*')
    {
        $user = [ 'model' => 'User', 'foreign_key' => AuthComponent::user('id')];
        return $this->Acl->check(
            $user,
               /* 'controllers/'.*/
            $path,
            $permission
        );
    }
}
