<?php

/**
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 *
 * Licensed under The CeCiLL V2 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Libriciel SCOP (http://www.libriciel.fr)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @license     http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html CeCiLL V2 License
 */

if (!empty(${'acos_'.$model})) {
    $this->BsForm->setLeft(0);
    $this->BsForm->setRight(12);

    $titles=[  ['title' => ''],
                ['title' => __d('auth_manager', 'permissions')]];
    foreach (${'permKeys_'.$model} as $key => $value) {
        $titles[]=['title' =>__d('auth_manager', $value),
                    'class'=> 'text-center'
        ];
    }
    $tableDroits= $this->Bs->table($titles, ['hover', 'striped'], ['class' => ['tableAco'.$model],'data-toggle'=>'table']);
    $configCRUD=Configure::read('AuthManager.configCRUD');

    $groups_cruds=[];
    foreach ($configCRUD['CRUD'] as $key => $value) {
        if (!empty($value['group']) && $value['group'] === true) {
            $groups_cruds[] = $key;
        }
    }


    $fullAllChecked=true;
    if (!empty(${'acos_'.$model})) {
        foreach (${'acos_'.$model} as $key_aco=>$aco) {
            if (!empty(${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id']) &&
            ${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id'] == $aco['Aco']['id'] &&
                    $aco['Aco']['id'] != 1) {
                $aco_select = $this->Bs->icon('arrow-circle-down');
            } else {
                $aco_select = $this->Bs->icon('arrow-right');
            }

            $label='';
            if ($aco['Aco']['parent_id']===1 || empty($aco['Aco']['parent_id'])) {//$aco['Aco']['parent_id'] != $aco_encour ||
                $label=$this->Bs->tag('b', __d('auth_manager', $aco['Action']));
            } else {
                $aco_select = $this->Bs->icon('arrow-right', [], ['style'=>'margin-left: '.substr_count($aco['Action'], '/') .'em']);
                $label=__d('auth_manager', $aco['Action']);
            }
            $this->Bs->lineAttributes(['data-model'=>$model, 'data-aco_id'=>$aco['Aco']['id'], 'data-parent_id'=>$aco['Aco']['parent_id']]);

            $tableDroits.= $this->Bs->cell($aco_select);


            // $tableDroits.= $this->Bs->cell($aco['Aco']['id']);

            $allChecked=true;
            foreach (${'permKeys_'.$model} as $key => $value) {
                // debug($aco['Aro']);
                if ($allChecked && (
                    empty($aco['Aro'][0]['Permission'][$value])
                xor (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] !== '1')
                )) {
                    $allChecked = false ;
                }
            }
            if (!$allChecked) {
                $fullAllChecked = false;
            }
            $checkbox = $this->BsForm->checkbox(
                'Aco.'.$model.'.'. $aco['Aco']['id'],
                [  'checked' => $allChecked,
                            'class'=> 'childCheckboxAuthManager',
                            'autocompete'=>false,
                            'data-model' => $model,
                            'data-aco_id' => $aco['Aco']['id'],
                            'title'=>__d('auth_manager', 'Donner tous les droits à la ligne'),
                            //'inline'=>'inline',
                            'label' => $label]
            );
            $checkbox.=$this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.alias', ['value' => $aco['Action']]);
            $checkbox.=$this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.foreign_key', ['value' => $aco['Aco']['foreign_key']]);
            $tableDroits.= $this->Bs->cell($checkbox);

//        if (!in_array(, $controllers_list)) {
//            foreach (array_diff(${'permKeys_' . $model}, array('_update','_create','_delete')) as $key => $value) {
//                $checkbox = $this->BsForm->checkbox('Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value, array(
//                    'checked' => (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] === '1') ? true : false,
//                    'inline' => 'inline',
//                    'autocompete' => false,
//                    'title' => __d('auth_manager', $value),
//                    'label' => false)
//                );
//
//                $tableDroits.= $this->Bs->cell($checkbox, 'text-center', array('colspan' => 4));
//                }
//                $this->Bs->linePosition(0);
//            } else {
            $diff=[];
            $intersect=[];
            $groupAll=false;
            if (in_array($aco['Aco']['alias'], $groups_cruds, true)) {
                $groupAll=true;
                $diff = array_diff(${'permKeys_' . $model}, ['_update','_create','_delete']);
                $intersect = array_intersect(${'permKeys_' . $model}, ['_update','_create','_delete']);
            }

            foreach (${'permKeys_' . $model} as $key => $value) {
                $checkbox = $this->BsForm->checkbox(
                    'Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value,
                    [
                        'checked' => (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] === '1') ? true : false,
                        'inline' => 'inline',
                        'style' => (in_array($value, $intersect, true)?'display:none':''),
                        'autocompete' => false,
                        "data-model" => $model,
                        "data-aco_id" => $aco['Aco']['id'],
                        'class'=> (($value=='_read'&& $groupAll)?'AuthManage_groupAllCRUDCheckbox':''),
                        'title' => __d('auth_manager', $value),
                        'label' => false]
                );

                $tableDroits.= $this->Bs->cell($checkbox, 'text-center '.($value=='_manager'?'danger':null));
            }
            //     }
        }
    } else {
        $tableDroits.= $this->Bs->cell(
            $this->Bs->tag('p', $this->Bs->icon('trash')
                   .' '.__('Aucun droit disponible')),
            'text-center',
            ['colspan'=> count(${'permKeys_'.$model})]
        );
    }

    $tableDroits.= $this->Bs->endTable();

    echo $this->Bs->tag(
        'h4',
        $this->BsForm->checkbox(
        'Aco.'.$model,
        [  'checked' => $fullAllChecked,
                        'class'=> 'masterCheckboxAuthManager',
                        'data-model'=>$model,
                        //'inline'=>'inline',
                        'title'=>__d('auth_manager', 'Donner tous les droits au tableau'),
                        'autocompete' => false,
                        'label' => $this->Bs->tag('span', __d('auth_manager', '%s permissions', __d('auth_manager', ${'acoAlias_'.$model})), ['class'=>'label label-primary'])
]
    )
    );

    echo $tableDroits;

    echo $this->Bs->scriptBlock(
        "
    require(['domReady'], function (domReady) {
        domReady(function() {
        require(['jquery', '/AuthManager/js/appAuthManager.js'], function ($) {
                $.appAuthManager();
            });
        });
    });"
    );
}
